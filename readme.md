# ascii-stream-generator [![pipeline status](https://gitlab.com/xhocht/ascii-stream-generator/badges/master/pipeline.svg)](https://gitlab.com/xhocht/ascii-stream-generator/commits/master)

The purpose of this package is, to create a readable stream, that generates ASCII characters, up to a given byte limit.

**This package is  currently under development and not in a stable state**

## Install
```
npm install ascii-stream-generator --save
````

## Test
To run the unit tests, execute following command:
```
npm run test
```

## Example
```
const stream = require('ascii-stream-generator');

stream.generate(100).pipe(process.stdout);
```
## License
[MIT](http://vjpr.mit-license.org)