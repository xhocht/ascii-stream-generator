const stream = require('./../src/stream');
const {expect} = require('chai');
const {Writable} = require('stream');

describe('Stream', () => {
    describe('_generateASCIIString', () => {
        it('should return a string with 1024 characters', () => {
            const result = stream._generateASCIIString(1024);
            expect(result).to.be.lengthOf(1024);
            expect(result).to.be.a('string');
        }); 

        it('should return a string with 4KB of characters', () => {
            const result = stream._generateASCIIString(1024*4);
            expect(result).to.be.lengthOf(1024*4);
            expect(result).to.be.a('string');
        });    

        it('should return a string that starts with !"#$%&\'()*', () => {
            const result = stream._generateASCIIString(10);
            expect(result).to.be.lengthOf(10);
            expect(result).to.be.equal('!"#$%&\'\()*');
        });   
    });

    describe('generate', () => {
        it('should return a stream of 100KB length', (done) => {
            let result = 0;
            const streamWriter = new Writable({
                write(chunk, encoding, callback) {
                    const buffer = Buffer.from(chunk);
                    result = result + buffer.length;
                    callback();
                }
            })
            const streamReader = stream.generate(1024*100);
            streamReader.pipe(streamWriter).on('finish', () => { 
                expect(result).to.be.equal(1024*100);
                done();
            });
            
        });
    });
})