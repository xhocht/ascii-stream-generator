const {Readable} = require('stream');

const generateASCIIString = (length) => 
    Array(length)
        .fill(0)
        .map((_, i) => String.fromCharCode((i % 94) + 33))
        .join('');

/**
 * Returns a readable stream, which streams ASCII data until the given length.
 * @param {length} number
 * @return {readablestream}
 */
const  generate = (length) => {
    let transmitted = 0;
    return new Readable({
        read(size) {
            const nextTansBlock = (length - transmitted) > size ? size : length - transmitted;
            this.push(generateASCIIString(nextTansBlock));

            if ((transmitted + nextTansBlock) === length) {
                this.push(null);
            } 
            transmitted = transmitted + nextTansBlock;
        }
    });
}

module.exports = {
    generate,
    _generateASCIIString: generateASCIIString
}